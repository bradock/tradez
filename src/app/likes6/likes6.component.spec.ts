import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Likes6Component } from './likes6.component';

describe('Likes6Component', () => {
  let component: Likes6Component;
  let fixture: ComponentFixture<Likes6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Likes6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Likes6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
