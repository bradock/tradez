import { Component } from '@angular/core';
import { Produto } from '../app/produto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TradeZ';
}

