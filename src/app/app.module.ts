import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProdutoComponent } from './produto/produto.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { AppRoutingModule } from './app-routing.module';
import { HistoricoComponent } from './historico/historico.component';
import { LikesComponent } from './likes/likes.component';
import { Likes2Component } from './likes2/likes2.component';
import { Likes3Component } from './likes3/likes3.component';
import { Likes4Component } from './likes4/likes4.component';
import { Likes5Component } from './likes5/likes5.component';
import { Likes6Component } from './likes6/likes6.component';
import { InicioComponent } from './inicio/inicio.component';

@NgModule({
  declarations: [
    AppComponent,
    ProdutoComponent,
    CadastroComponent,
    HistoricoComponent,
    LikesComponent,
    Likes2Component,
    Likes3Component,
    Likes4Component,
    Likes5Component,
    Likes6Component,
    InicioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
