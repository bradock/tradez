import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Likes3Component } from './likes3.component';

describe('Likes3Component', () => {
  let component: Likes3Component;
  let fixture: ComponentFixture<Likes3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Likes3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Likes3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
