import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Likes5Component } from './likes5.component';

describe('Likes5Component', () => {
  let component: Likes5Component;
  let fixture: ComponentFixture<Likes5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Likes5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Likes5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
