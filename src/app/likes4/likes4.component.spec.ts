import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Likes4Component } from './likes4.component';

describe('Likes4Component', () => {
  let component: Likes4Component;
  let fixture: ComponentFixture<Likes4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Likes4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Likes4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
