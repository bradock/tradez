import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroComponent } from './cadastro/cadastro.component';
import { ProdutoComponent } from './produto/produto.component';
import { HistoricoComponent } from './historico/historico.component';
import { LikesComponent } from './likes/likes.component';
import { Likes2Component } from './likes2/likes2.component';
import { Likes3Component } from './likes3/likes3.component';
import { Likes4Component } from './likes4/likes4.component';
import { Likes5Component } from './likes5/likes5.component';
import { Likes6Component } from './likes6/likes6.component';
import { InicioComponent } from './inicio/inicio.component';
import { AppComponent} from './app.component';

const routes: Routes = [

  
  {path: 'cadastro', component: CadastroComponent},
  {path: 'produto', component: ProdutoComponent},
  {path: 'historico', component: HistoricoComponent},
  {path: 'likes', component: LikesComponent},
  {path: 'likes2', component: Likes2Component},
  {path: 'likes3', component: Likes3Component},
  {path: 'likes4', component: Likes4Component},
  {path: 'likes5', component: Likes5Component},
  {path: 'likes6', component: Likes6Component},
  {path: 'home', component: InicioComponent},
  {path: '', component: InicioComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
  
})
export class AppRoutingModule { }
